import 'package:flutter/material.dart';
import 'pages.dart';

class MyApp extends StatefulWidget {
  @override
  const MyApp({Key? key}) : super(key: key);
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'My First App',
      home: LoginPage(),
    );
  }

  @override
  State<_MyAppState> createState() => _MyAppState();
}

class _MyAppState extends MyApp {
  const _MyAppState({Key? key}) : super(key: key);

  Widget buildPage(String text, Color color) {
    return Center(
      child: Text(
        text,
        style: TextStyle(fontSize: 20.0, color: color),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(backgroundColor: Colors.cyan),
      body: buildPageView(),
    );
  }
}

Widget buildPageView() {
  return PageView(
    children: [
      <Widget>[buildPage('text', Colors.black)]
    ],
  );
}
